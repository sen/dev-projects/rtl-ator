#include "systemc.h"
#include "verilated_vcd_sc.h"

// V{RTL_TOP}.h
#include "Vtop.h"

#define TRACE

int sc_main(int argc, char * argv[])
{

#ifdef TRACE
   // Verilator trace file
   Verilated::traceEverOn(true);
   VerilatedVcdSc* tfp = new VerilatedVcdSc;
#endif

   sc_time T(10,SC_NS);

   const unsigned int STEPS     = 256;
   const unsigned int MAIN_DIV  = 4;
   const unsigned int DIV_RATIO = 1;

   sc_time Tsim = T * STEPS * MAIN_DIV * DIV_RATIO * 1024 * 2;

   sc_clock clk("clk",T);
   sc_signal<bool> nrst("nrst");
   sc_signal<bool> pwm ("pwm");

   Vtop dut("top_verilog");

   dut.clk (clk);
   dut.nrst(nrst);
   dut.pwm(pwm);

#ifdef TRACE
   sc_start(SC_ZERO_TIME);
   // Verilator trace file, depth
   dut.trace(tfp, 10);
   tfp->open("simu.vcd");
#endif

   nrst = 0;
   sc_start(10*T);
   nrst = 1;
   sc_start(Tsim);

#ifdef TRACE
   tfp->close();
#endif
   return 0;
}
